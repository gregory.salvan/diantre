if exists(':Diantre')
    finish
endif
runtime plugin/diantre/complete.v

function! s:func_name(tasks, task, context, group, version) abort
    let l:version = a:version
    if l:version == -1 
        let l:version = 0
        let l:varname = 'g:' . join([a:context, a:group, a:task, 'version'], '#') 
        if exists(l:varname)
            let l:version = eval(l:varname)
        endif
    endif
    return a:tasks[a:context][a:group][l:version]
endfunction

function! s:execute_task(task, ...)
    let [l:task, l:version] = g:diantre.split_version(a:task, -1)
    let l:contexts = Diantre_all_contexts()
    let l:groups = Diantre_all_groups() 
    let l:tasks = g:diantre.tasks(l:task)
    for l:context in l:contexts
        if has_key(l:tasks, l:context) && !empty(a:000) && has_key(l:tasks[l:context], a:1)
            return call(s:func_name(l:tasks, l:task, l:context, a:1, l:version), a:000[1:])
        endif
    endfor
    for context in l:contexts
        if has_key(l:tasks, l:context)
            for l:group in l:groups
                if has_key(l:tasks[l:context], l:group)
                    return call(s:func_name(l:tasks, l:task, l:context, l:group, l:version), a:000)
                endif
            endfor
        endif
    endfor
    if has_key(l:tasks, a:1)
        for l:group in l:groups
            if has_key(l:tasks[a:1], l:group)
                return call(s:func_name(l:tasks, l:task, a:1, l:group, l:version), a:000[1:])
            endif
        endfor
    endif
    if a:0 > 1 && has_key(l:tasks, a:1) && has_key(l:tasks[a:1], a:2)
        return call(s:func_name(l:tasks, l:task, a:1, a:2, l:version), a:000[2:])
    endif
    let l:all_tasks = []
    for context in keys(l:tasks)
        for group in keys(l:tasks[context])
            let l:all_tasks = add(l:all_tasks, a:task . ' ' . context . ' ' . group )
        endfor
    endfor
    let l:task_name = a:task . ' ' . get(a:000, 0, '<no context>') . ' ' . get(a:000, 1, '<no group>')
    throw 'Could not find task: ' . l:task_name  . "\nexisting tasks:\n\t" . join(l:all_tasks, "\n\t")
endfunction

command! -nargs=+ -complete=custom,s:diantre_complete Diantre :call s:execute_task(<f-args>) 
ca di Diantre
