if exists(':Diantre')
    finish
endif

function! s:complete_contexts(words, cur_pos, ...)
    let l:result = get(a:000, 0, [])
    return extend(l:result, g:diantre.contexts(get(a:words, 1, '')))
endfunction

function! s:complete_groups(words, cur_pos, ...)
    let l:result = get(a:000, 0, [])
    let l:task = g:diantre.tasks(get(a:words, 1, ''))
    let l:context = get(a:words, 2, '')
    if has_key(l:task, l:context)
        call extend(l:result, keys(l:task[l:context]))
    endif
    return l:result
endfunction

function! s:complete_tasks()
    return keys(g:diantre.tasks())
endfunction

function! s:complete_name(task, context, group)
    let l:fname = join([a:context, a:group, a:task], '#') 
    if exists('g:' . l:fname . '#version')
        let l:fname = l:fname . '__' . eval('g:' . l:fname . '#version')
    endif
    return l:fname . '__complete'
endfunction

function! s:complete_parameters(words, cur_pos, ...)
    let l:result = get(a:000, 0, [])
    let l:fname = s:complete_name(a:words[1], a:words[2], a:words[3])
    if exists('*' . l:fname)
        return call(function(l:fname), [a:words, a:cur_pos, l:result])
    endif
    return l:result
endfunction

function! s:complete_env_contexts_groups(words, cur_pos, groups, ...)
    let l:result = get(a:000, 0, [])
    for context in Diantre_all_contexts()
        let l:cwords = add(a:words[:1], context) + a:words[2:-2] 
        for group in a:groups 
            let l:gwords = add(l:cwords, group) 
            let l:result = s:complete_parameters(l:gwords, a:cur_pos, l:result)
        endfor
        let l:result = s:complete_groups(l:cwords, a:cur_pos, l:result)
    endfor
    return l:result
endfunction

function! s:complete_env_contexts_parameters(words, cur_pos, ...)
    let l:result = get(a:000, 0, [])
    for context in Diantre_all_contexts()
        let l:cwords = add(a:words[:1], context) + a:words[2:-2] 
        let l:result = s:complete_parameters(l:cwords, a:cur_pos, l:result)
    endfor
    return l:result
endfunction

function! s:complete_env_groups(words, cur_pos, ...)
    let l:result = get(a:000, 0, [])
    for group in Diantre_all_groups()
        let l:gwords = add(a:words[:2], group) 
        let l:result = s:complete_parameters(l:gwords, a:cur_pos, l:result)
    endfor
    return l:result
endfunction

function! s:split_cmd_line(arg_lead, cmd_line, cur_pos)
    let l:index = len(split(a:cmd_line[:a:cur_pos], '[^ ]\+'))
    if a:cur_pos == len(a:cmd_line)
        let l:words = (empty(a:arg_lead) ? add(split(a:cmd_line, ' '), a:arg_lead) : split(a:cmd_line, ' '))
        return [l:index, l:words]
    endif
    return [l:index, split(a:cmd_line, ' ')]
endfunction

function! s:diantre_complete(arg_lead, cmd_line, cur_pos) 
    if a:cur_pos < 8 || (a:cur_pos == 8 && len(a:cmd_line) > a:cur_pos)  
        return ''
    endif
    let [l:pos, l:words] = s:split_cmd_line(a:arg_lead, a:cmd_line, a:cur_pos)
    if l:pos == 1
        return g:diantre.format(s:complete_tasks())
    endif
    if l:pos == 2
        let l:result = s:complete_env_contexts_groups(l:words, l:pos, Diantre_all_groups())
        return g:diantre.format(s:complete_contexts(l:words, l:pos, l:result))
    endif
    if l:pos == 3
        let l:result = s:complete_env_groups(l:words, l:pos, s:complete_env_contexts_parameters(l:words, l:pos))
        return g:diantre.format(s:complete_groups(l:words, l:pos, l:result))
    endif
    return g:diantre.format(s:complete_parameters(l:words, l:pos))
endfunction
